use std::sync::Arc;

use im::hashmap::HashMap;
use im::hashset::HashSet;

use types::{
    Type,Environment,Substitution,FTVs,ApplySub,
    Scheme,Constructor,
    empty_substitution, singleton_substitution,
    func_t,compose,int_t,bool_t,char_t,update_env,
};
#[cfg(test)]
use types::t_var;
use expression::{Lit,Expr,Pattern};

#[derive(Debug, PartialEq)]
pub enum Err {
    UndefinedVar(Arc<String>),
    CompilerBug,
    NotAFunction,
    OccursCheck,
    CannotUnify,
    BadPattern,
}

pub struct Inference {
    nvars: u32,
    constructors: HashMap<String, Constructor>,
}

pub fn infer(expr: Arc<Expr>) -> Result<Type, Err> {
    let mut inference = Inference::new();
    let env = HashMap::new();
    let (typ, sub) = inference.infer_type(&env, expr)?;
    Ok(typ.apply_sub(&sub))
}

impl Inference {
    pub fn new() -> Self {
        Inference{nvars: 0, constructors: HashMap::new()}
    }

    pub fn add_constructor(&mut self, name: String, constr: Constructor) {
        self.constructors = self.constructors.insert(name, constr);
    }

    pub fn infer_type(&mut self, env: &Environment, expr: Arc<Expr>)
                  -> Result<(Type, Substitution), Err> {
        match (*expr).clone() {
            Expr::Lit(l) => infer_lit(l),

            Expr::Var(v) => match env.get(&*v) {
                Some(scheme) =>
                    Ok((self.instantiate(scheme), empty_substitution())),
                None         =>
                    Err(Err::UndefinedVar(v)),
            },

            Expr::App(e1, e2) => {
                let return_type = self.next_var();
                let (t1, sub1) = self.infer_type(env, e1)?;
                let (t2, sub2) = self.infer_type(&env.apply_sub(&sub1), e2)?;
                let sub3 = most_general_unifier(
                    &t1.apply_sub(&sub2),
                    &func_t(t2, return_type.clone())
                )?;
                let composed = compose(&compose(&sub1, &sub2), &sub3);
                Ok((return_type.apply_sub(&sub3), composed))
            },

            Expr::Let(bindings, e) => {
                // Create a type variable for each variable bound:
                let binding_vars: HashMap<String, Type> = bindings.iter()
                    .map(|ptr| ((*ptr).0.clone(), self.next_var()))
                    .collect();

                // Create an environment where all the bindings are defined
                let mut recursive_env = binding_vars.iter()
                    .fold((*env).clone(),
                          |new_env, (name, typ)| update_env(&new_env, name, typ.as_scheme()));

                // ...and use that to find the type of the recursive functions
                let mut types = HashMap::new();
                let mut current_sub = HashMap::new();
                for arc in bindings {
                    let (var, expr) = (*arc).clone();
                    recursive_env = recursive_env.apply_sub(&current_sub);
                    let (t, sub) = self.infer_type(&recursive_env, Arc::new(expr))?;
                    recursive_env = recursive_env.insert(var.clone(), t.as_scheme());
                    types = types.insert(var, t);
                    current_sub = compose(&current_sub, &sub);
                }

                // Then, re-create the environment with more general type
                // schemes for the functions (generalizing them against the
                // original type environment with the substitution applied to
                // it).
                let mut env2 = env.apply_sub(&current_sub);
                for (var, t) in types {
                    let scheme = self.generalize(&env2, t.apply_sub(&current_sub));
                    env2 = env2.insert((*var).clone(), scheme);
                }

                // Finally, use that new environment to find the type of `e`.
                return self.infer_type(&env2, e);
            },

            Expr::Abs(v, e) => {
                let var_type = self.next_var();
                let scheme = Scheme{vars: HashSet::new(), t: var_type.clone()};
                let body_env = update_env(env, v, scheme);
                let (t1, sub1) = self.infer_type(&body_env, e)?;
                Ok((func_t(var_type.apply_sub(&sub1), t1), sub1))
            },

            Expr::If(test, if_case, else_case) => {
                // Require that the test be boolean
                let (test_type, sub1) = self.infer_type(env, test)?;
                let sub2 = most_general_unifier(&test_type, &bool_t())?;
                let mut sub = compose(&sub1, &sub2);

                // Type both cases
                let (if_type, sub3) = self.infer_type(&env.apply_sub(&sub), if_case)?;
                sub = compose(&sub, &sub3);
                let (else_type, sub4) = self.infer_type(&env.apply_sub(&sub), else_case)?;
                sub = compose(&sub, &sub4);

                // Require that those two have the same type
                let sub5 = most_general_unifier(&if_type, &else_type)?;
                sub = compose(&sub, &sub5);

                Ok((else_type.apply_sub(&sub5), sub))
            },

            Expr::Match(expr, arms) => {
                let result_type = self.next_var();

                // Find the type of the matched value
                let (expr_type, sub1) = self.infer_type(env, expr)?;

                let mut current_sub = sub1;

                for arm in arms {
                    let (pattern, e) = (*arm).clone();
                    let (arm_env, pat_sub) = self.infer_pattern(
                        pattern,
                        expr_type.apply_sub(&current_sub),
                        env
                    )?;
                    current_sub = compose(&current_sub, &pat_sub);

                    let (arm_type, expr_sub) = self.infer_type(
                        &arm_env, Arc::new(e))?;
                    current_sub = compose(&current_sub, &expr_sub);

                    let sub = most_general_unifier(
                        &arm_type.apply_sub(&current_sub),
                        &result_type
                    )?;
                    current_sub = compose(&current_sub, &sub);
                }

                Ok((result_type.apply_sub(&current_sub), current_sub))
            },
        }
    }

    // this assumes that the names in pattern have already been verified
    // to be unique
    fn infer_pattern(&mut self, pat: Pattern, t: Type, env: &Environment)
                     -> Result<(Environment, Substitution), Err> {
        match pat {
            Pattern::Wildcard =>
                Ok((env.clone(), empty_substitution())),

            Pattern::Var(name) => {
                let sch = self.generalize(env, t);
                Ok((update_env(env, Arc::new(name), sch),
                    empty_substitution()))
            },

            Pattern::Con(constructor, children) => {
                let constr = match self.constructors.get(&constructor) {
                    Some(constr) =>
                        Ok(constr),
                    None =>
                        Err(Err::UndefinedVar(Arc::new(constructor))),
                }?;

                let t2 = self.instantiate(constr.sch.clone());

                // Unify t2 with the actual type t to find out what substitution
                // to apply
                let mut current_sub = most_general_unifier(&t2, &t)?;

                if children.len() != constr.fields.len() {
                    return Err(Err::BadPattern);
                }

                let mut updated_env = env.clone();
                for i in 0..children.len() {
                    let (env2, sub) = self.infer_pattern(
                        (*children.get(i).unwrap()).clone(),
                        constr.fields.get(i).unwrap().apply_sub(&current_sub),
                        &updated_env
                    )?;
                    updated_env = env2;
                    current_sub = compose(&current_sub, &sub);
                }

                Ok((updated_env, current_sub))
            },
        }
    }

    fn generalize(&self, env: &Environment, t: Type) -> Scheme {
        Scheme{
            vars: t.ftvs().difference(env.ftvs()),
            t: t,
        }
    }

    fn instantiate(&mut self, scheme: Arc<Scheme>) -> Type {
        let sub = scheme.vars.iter()
            .map(|v| (v.clone(), self.next_var()))
            .collect();
        scheme.t.apply_sub(&sub)
    }

    fn next_var(&mut self) -> Type {
        let var = self.nvars.to_string();
        self.nvars += 1;
        Type::Var(Arc::new(var))
    }
}

fn infer_lit(lit: Lit) -> Result<(Type, Substitution), Err> {
    Ok(match lit {
        Lit::I(_) => (int_t(), empty_substitution()),
        Lit::B(_) => (bool_t(), empty_substitution()),
        Lit::C(_) => (char_t(), empty_substitution()),
    })
}

fn most_general_unifier(t1: &Type, t2: &Type) -> Result<Substitution, Err> {
    if t1 == t2 {
        return Ok(empty_substitution());
    } else if let Type::Var(v) = t1 {
        return replace(v, t2)
    } else if let Type::Var(v) = t2 {
        return replace(v, t1)
    } else if let (Type::App(l1, r1), Type::App(l2, r2)) = (t1, t2) {
        let sub1 = most_general_unifier(&l1, &l2)?;
        let sub2 = most_general_unifier(&r1.apply_sub(&sub1), &r2.apply_sub(&sub1))?;
        return Ok(compose(&sub1, &sub2));
    }

    Err(Err::CannotUnify)
}

#[test]
fn mgu_test() {
    assert_eq!(
        Err(Err::CannotUnify),
        most_general_unifier(&int_t(), &bool_t()));
    assert_eq!(
        Ok(empty_substitution()),
        most_general_unifier(&int_t(), &int_t()));
    assert_eq!(
        Ok(empty_substitution()),
        most_general_unifier(&t_var("12"), &t_var("12")));
    assert_eq!(
        Ok(singleton_substitution("12".to_owned(), t_var("33"))),
        most_general_unifier(&t_var("12"), &t_var("33")));
    assert_eq!(
        Ok(singleton_substitution("12".to_owned(), int_t())),
        most_general_unifier(&t_var("12"), &int_t()));
    assert_eq!(
        Ok(singleton_substitution("12".to_owned(), int_t())),
        most_general_unifier(&int_t(), &t_var("12")));
    assert_eq!(
        Ok(hashmap![
            "1".to_owned() => int_t(),
            "0".to_owned() => int_t()
        ]),
        most_general_unifier(
            &func_t(t_var("1"), t_var("1")),
            &func_t(t_var("0"), int_t()),
        ));
}

fn replace(var: &String, t: &Type) -> Result<Substitution, Err> {
    if t.ftvs().contains(var) {
        return Err(Err::OccursCheck);
    }

    return Ok(singleton_substitution(var.clone(), t.clone()));
}
