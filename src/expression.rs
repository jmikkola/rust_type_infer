use std::collections::HashSet;
use std::sync::Arc;

use im::vector::Vector;

use parse::{parse, AST};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Lit {
    I(i64),
    B(bool),
    C(char),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Expr {
    Lit(Lit),
    Var(Arc<String>),
    App(Arc<Expr>, Arc<Expr>),
    Let(Vector<(String, Expr)>, Arc<Expr>),
    Abs(Arc<String>, Arc<Expr>),
    If(Arc<Expr>, Arc<Expr>, Arc<Expr>),
    Match(Arc<Expr>, Vector<(Pattern, Expr)>),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Pattern {
    Wildcard,
    Var(String),
    Con(String, Vector<Pattern>),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Err {
    UnknownExpression,
    InvalidBindings,
    DuplicateBinding,
    UnknownPattern,
    BadIfExpr,
    BadLetExpr,
    BadFnExpr,
    BadMatchExpr,
}

pub fn from_asts(asts: Vector<AST>) -> Result<Vector<Expr>, Err> {
    let mut expressions = Vector::new();

    for ast in asts {
        let expression = from_ast(ast)?;
        expressions = expressions.push_back(expression);
    }

    Ok(expressions)
}

pub fn from_ast(ast: Arc<AST>) -> Result<Expr, Err> {
    match (*ast).clone() {
        AST::Atom(s) => Ok(
            if *s == "false" {
                Expr::Lit(Lit::B(false))
            } else if *s == "true" {
                Expr::Lit(Lit::B(true))
            } else if let Some(i) = s.parse().ok() {
                Expr::Lit(Lit::I(i))
            } else {
                Expr::Var(s)
            }
        ),
        AST::Cons(list) => from_list(list),
    }
}

fn from_list(list: Vector<AST>) -> Result<Expr, Err> {
    if list.len() == 0 {
        Err(Err::UnknownExpression)
    } else if let AST::Atom(_) = list[0] {
        if list[0] == atom("let") {
            let (bindings, expr) = let_structured(list.clone())?;
            let bs = recognize_bindings(bindings)?;
            let e = from_ast(expr)?;
            Ok(Expr::Let(bs, Arc::new(e)))
        } else if list[0] == atom("fn") {
            let (var, expr) = fn_structured(list.clone())?;
            let e = from_ast(expr)?;
            Ok(Expr::Abs(var, Arc::new(e)))
        } else if list[0] == atom("if") {
            let (expr1, expr2, expr3) = if_structured(list.clone())?;
            let e1 = from_ast(expr1)?;
            let e2 = from_ast(expr2)?;
            let e3 = from_ast(expr3)?;
            Ok(Expr::If(Arc::new(e1), Arc::new(e2), Arc::new(e3)))
        } else if list[0] == atom("match") {
            let (expr, arms) = match_structured(list.clone())?;
            let e1 = from_ast(expr)?;
            let match_arms = recognize_match_arms(arms)?;
            Ok(Expr::Match(Arc::new(e1), match_arms))
        } else {
            build_application(list)
        }
    } else {
        build_application(list)
    }
}

fn build_application(list: Vector<AST>) -> Result<Expr, Err> {
    if list.len() < 2 {
        return Err(Err::UnknownExpression);
    }

    let mut expr = from_ast(list.get_unwrapped(0))?;
    for node in list.tail().unwrap() {
        let arg = from_ast(node)?;
        expr = Expr::App(Arc::new(expr), Arc::new(arg));
    }

    return Ok(expr);
}

fn recognize_match_arms(list: Vector<AST>)
                        -> Result<Vector<(Pattern, Expr)>, Err> {
    let mut pairs = vector![];
    for arm in list {
        if let AST::Cons(items) = (*arm).clone() {
            if items.len() == 2 {
                let pat = recognize_pattern(items[0].clone())?;
                let expr = from_ast(items.get_unwrapped(1))?;
                pairs = pairs.push_back((pat, expr));
            } else {
                return Err(Err::UnknownExpression);
            }
        } else {
            return Err(Err::UnknownExpression);
        }
    }
    Ok(pairs)
}

fn recognize_pattern(node: AST) -> Result<Pattern, Err> {
    match node {
        AST::Atom(s) => {
            if *s == "_".to_owned() {
                Ok(Pattern::Wildcard)
            } else if (*s).starts_with(char::is_uppercase) {
                Ok(Pattern::Con((*s).clone(), vector![]))
            } else {
                Ok(Pattern::Var((*s).clone()))
            }
        }
        AST::Cons(items) => {
            let mut result = Err(Err::UnknownPattern);
            if items.len() > 0 {
                if let AST::Atom(s) = items[0].clone() {
                    if (*s).starts_with(char::is_uppercase) {
                        let mut nested = vector![];
                        for n in items.skip(1) {
                            let p = recognize_pattern((*n).clone())?;
                            nested = nested.push_back(p);
                        }
                        result = Ok(Pattern::Con((*s).clone(), nested));
                    }
                }
            }
            result
        }
    }
}

fn recognize_bindings(list: Vector<AST>) -> Result<Vector<(String, Expr)>, Err> {
    if list.len() % 2 != 0 {
        return Err(Err::InvalidBindings);
    }

    let mut bindings = Vector::new();
    let mut seen = HashSet::new();
    let mut previous: Option<Arc<String>> = None;

    for node in list {
        if let Some(var) = previous {
            let e = from_ast(node)?;
            bindings = bindings.push_back(((*var).clone(), e));
            previous = None;
        } else {
            if let AST::Atom(var) = (*node).clone() {
                if seen.contains(&*var) {
                    return Err(Err::DuplicateBinding);
                }
                seen.insert(var.clone());
                previous = Some(var);
            } else {
                return Err(Err::InvalidBindings);
            }
        }
    }

    Ok(bindings)
}

fn let_structured(list: Vector<AST>)
                  -> Result<(Vector<AST>, Arc<AST>), Err> {
    if list.len() == 3 && list[0] == atom("let") {
        if let AST::Cons(items) = list[1].clone() {
            return Ok((items, list.get_unwrapped(2)));
        }
    }
    Err(Err::BadLetExpr)
}

fn fn_structured(list: Vector<AST>)
                 -> Result<(Arc<String>, Arc<AST>), Err> {
    if list.len() == 3 && list[0] == atom("fn") {
        if let AST::Atom(var) = list[1].clone() {
            return Ok((var, list.get_unwrapped(2)));
        }
    }
    Err(Err::BadFnExpr)
}

fn match_structured(list: Vector<AST>)
                    -> Result<(Arc<AST>, Vector<AST>), Err> {
    if list.len() >= 2 && list[0] == atom("match") {
        return Ok((list.get_unwrapped(1), list.skip(2)));
    }
    Err(Err::BadMatchExpr)
}

fn if_structured(list: Vector<AST>)
                 -> Result<(Arc<AST>, Arc<AST>, Arc<AST>), Err> {
    if list.len() == 4 && list[0] == atom("if") {
        return Ok((list.get_unwrapped(1),
                     list.get_unwrapped(2),
                     list.get_unwrapped(3)));
    }
    Err(Err::BadIfExpr)
}

fn atom(s: &str) -> AST {
    AST::Atom(Arc::new(s.to_owned()))
}

#[cfg(test)]
fn var(s: &str) -> Expr {
    Expr::Var(Arc::new(s.to_owned()))
}

#[cfg(test)]
fn lit_i64(i: i64) -> Expr {
    Expr::Lit(Lit::I(i))
}

#[cfg(test)]
fn arc_str(s: &str) -> Arc<String> {
    Arc::new(s.to_owned())
}

#[test]
fn from_ast_test() {
    let id_fn = AST::Cons(vector![atom("fn"), atom("x"), atom("x")]);
    let ast = AST::Cons(vector![
        atom("let"),
        AST::Cons(vector![atom("id"), id_fn]),
        AST::Cons(vector![AST::Cons(vector![atom("id"), atom("id")]), atom("123")])
    ]);
    let expected = Expr::Let(
        vector![("id".to_owned(), Expr::Abs(arc_str("x"), Arc::new(var("x"))))],
        Arc::new(Expr::App(
            Arc::new(Expr::App(Arc::new(var("id")), Arc::new(var("id")))),
            Arc::new(lit_i64(123))
        )),
    );
    assert_eq!(Ok(expected), from_ast(Arc::new(ast)));
}

#[test]
fn test_recognize_pattern() {
    use self::Pattern::*;
    assert_eq!(Ok(Wildcard), recognize_pattern(atom("_")));
    assert_eq!(
        Ok(Var("xyz".to_owned())),
        recognize_pattern(atom("xyz"))
    );
    assert_eq!(
        Ok(Con("Nil".to_owned(), vector![])),
        recognize_pattern(atom("Nil"))
    );
    assert_eq!(
        Ok(Con("Pair".to_owned(), vector![Var("x".to_owned()), Wildcard])),
        recognize_pattern(AST::Cons(vector![atom("Pair"), atom("x"), atom("_")]))
    );
}

#[test]
fn test_parsing_match_arms() {
    let pattern1 = atom("Nil");
    let pattern2 = AST::Cons(vector![atom("Cons"), atom("n"), atom("_")]);
    let arm1 = AST::Cons(vector![pattern1, atom("123")]);
    let arm2 = AST::Cons(vector![pattern2, atom("456")]);
    let ast = AST::Cons(vector![atom("match"), atom("x"), arm1, arm2]);

    let p1 = Pattern::Con("Nil".to_owned(), vector![]);
    let e1 = Expr::Lit(Lit::I(123));
    let p2 = Pattern::Con(
        "Cons".to_owned(),
        vector![Pattern::Var("n".to_string()), Pattern::Wildcard]
    );
    let e2 = Expr::Lit(Lit::I(456));
    let expected = Expr::Match(
        Arc::new(Expr::Var(arc_str("x"))),
        vector![(p1, e1), (p2, e2)]
    );

    assert_eq!(Ok(expected), from_ast(Arc::new(ast)));
}

#[test]
fn test_parsing_match() {
    let text = "
        (match (Cons 123 Nil)
            ((Cons n _) n)
            (Nil 2))
    ";
    let ast = parse(text.to_owned());
    assert!(ast.is_ok());
    let astVec = ast.unwrap();
    assert_eq!(astVec.len(), 1);
    let root = astVec.get(0).unwrap();

    use self::Expr::*;

    let expected = Match(
        Arc::new(App(
            Arc::new(App(
                Arc::new(var("Cons")),
                Arc::new(lit_i64(123))
            )),
            Arc::new(var("Nil"))
        )),
        vector![
            (
                Pattern::Con("Cons".to_owned(), vector![
                    Pattern::Var("n".to_owned()),
                    Pattern::Wildcard]),
                var("n")
            ),
            (
                Pattern::Con("Nil".to_owned(), vector![]),
                lit_i64(2)
            )
        ]
    );

    assert_eq!(Ok(expected), from_ast(root));
}
