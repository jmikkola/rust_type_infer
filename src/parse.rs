use std::iter::Peekable;
use std::sync::Arc;

use im::vector::Iter;
use im::vector::Vector;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AST {
    Atom(Arc<String>),
    Cons(Vector<AST>),
}

impl AST {
    pub fn show(&self) -> String {
        match self {
            AST::Atom(s) => (**s).clone(),
            AST::Cons(elements) => {
                let mut s = "(".to_owned();
                for (i, elem) in elements.iter().enumerate() {
                    if i > 0 {
                        s.push(' ');
                    }
                    s += elem.show().as_str();
                }
                s.push(')');
                s
            },
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Err {
    ClosingUnopened,
    Unclosed,
}

#[derive(PartialEq)]
enum TokenState {
    Start,
    InComment,
    InAtom,
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum Token {
    LeftParen,
    RightParen,
    Atom(Arc<String>),
}

pub fn parse(s: String) -> Result<Vector<AST>, Err> {
    recognize(tokenize(s))
}

#[cfg(test)]
fn parse_and_show(s: String) -> Result<String, Err> {
    let mut result = String::new();
    for expr in parse(s)? {
        if !result.is_empty() {
            result.push(' ');
        }
        result += expr.show().as_str();
    }
    Ok(result)
}

#[test]
fn parse_test() {
    assert_eq!(
        Err(Err::Unclosed),
        parse_and_show("(foo) (;in a comment)".to_owned())
    );

    assert_eq!(
        Err(Err::ClosingUnopened),
        parse_and_show("(foo) (duplicate closing)) foo".to_owned())
    );

    assert_eq!(
        Ok("(= bar (* (+ 2 3) 3))".to_owned()),
        parse_and_show("(= bar (* (+ 2 3) 3))".to_owned())
    );

    assert_eq!(
        Ok("(= bar (* (+ 2 3) 3))".to_owned()),
        parse_and_show("(    =  ;comment\n bar (* (+ 2 3) 3  ))  ".to_owned())
    );

    assert_eq!(
        Ok("(match ((Cons 123) Nil) ((Cons n _) n) (Nil 2))".to_owned()),
        parse_and_show("
            (match ((Cons 123) Nil)
                ((Cons n _) n)
                (Nil 2))
            ".to_owned()
        )
    );
}

/// recognize converts a stream of tokens into a S-expressions
fn recognize(ts: Vector<Token>) -> Result<Vector<AST>, Err> {
    let mut expressions = Vector::new();
    let mut iterator = ts.iter().peekable();

    while let Some(token) = iterator.next() {
        match (*token).clone() {
            Token::LeftParen => {
                expressions = expressions.push_back(recognize_list(&mut iterator)?);
            },
            Token::RightParen => {
                return Err(Err::ClosingUnopened);
            },
            Token::Atom(s) => {
                expressions = expressions.push_back(AST::Atom(s));
            },
        }
    }

    Ok(expressions)
}

fn recognize_list(iterator: &mut Peekable<Iter<Token>>) -> Result<AST, Err> {
    let mut expressions = Vector::new();

    while let Some(token) = iterator.next() {
        match (*token).clone() {
            Token::LeftParen => {
                expressions = expressions.push_back(recognize_list(iterator)?);
            },
            Token::RightParen => {
                return Ok(AST::Cons(expressions));
            },
            Token::Atom(s) => {
                expressions = expressions.push_back(AST::Atom(s));
            },
        }
    }

    Err(Err::Unclosed)
}

#[test]
fn recognize_simple() {
    assert_eq!(Ok(vector![]), recognize(vector![]));

    assert_eq!(
        Ok(vector![ast_atom("foo")]),
        recognize(vector![atom("foo")])
    );

    assert_eq!(
        Ok(vector![AST::Cons(vector![])]),
        recognize(vector![Token::LeftParen, Token::RightParen])
    );

    assert_eq!(
        Ok(vector![
            AST::Cons(vector![
                ast_atom("foo"),
                AST::Cons(vector![])
            ]),
            ast_atom("bar"),
            AST::Cons(vector![])
        ]),
        recognize(vector![
            Token::LeftParen,
            atom("foo"),
            Token::LeftParen,
            Token::RightParen,
            Token::RightParen,
            atom("bar"),
            Token::LeftParen,
            Token::RightParen
        ])
    );
}

#[test]
fn recognize_errors() {
    assert_eq!(Err(Err::Unclosed), recognize(vector![Token::LeftParen]));
    assert_eq!(Err(Err::ClosingUnopened), recognize(vector![Token::RightParen]));
}

fn tokenize(s: String) -> Vector<Token> {
    let mut tokens = Vector::new();
    let mut state = TokenState::Start;
    let mut current = String::new();

    for c in s.chars() {
        match state {
            TokenState::Start => {
                match c {
                    ';' => { state = TokenState::InComment; },
                    '(' => { tokens = tokens.push_back(Token::LeftParen); },
                    ')' => { tokens = tokens.push_back(Token::RightParen); },
                    _   => {
                        if !c.is_whitespace() {
                            state = TokenState::InAtom;
                            current = c.to_string();
                        }
                    },
                }
            },

            TokenState::InComment => {
                if c == '\n' {
                    state = TokenState::Start;
                }
            },

            TokenState::InAtom => {
                if c == ';' || c == '(' || c == ')' || c.is_whitespace() {
                    tokens = tokens.push_back(Token::Atom(Arc::new(current)));
                    current = String::new();
                }

                match c {
                    ';' => {
                        state = TokenState::InComment;
                    },
                    '(' => {
                        tokens = tokens.push_back(Token::LeftParen);
                        state = TokenState::Start;
                    },
                    ')' => {
                        tokens = tokens.push_back(Token::RightParen);
                        state = TokenState::Start;
                    },
                    _ if c.is_whitespace() => {
                        state = TokenState::Start;
                    },
                    _   => {
                        current.push(c);
                    },
                }
            },
        }
    }

    if state == TokenState::InAtom {
        tokens = tokens.push_back(Token::Atom(Arc::new(current)));
    }

    return tokens;
}

#[cfg(test)]
fn atom(s: &str) -> Token {
    Token::Atom(Arc::new(s.to_owned()))
}

#[cfg(test)]
fn ast_atom(s: &str) -> AST {
    AST::Atom(Arc::new(s.to_owned()))
}

#[test]
fn tokenize_simple() {
    let expected = vector![
        Token::LeftParen,
        atom("asdf"),
        Token::LeftParen,
        atom("1"),
        Token::RightParen,
        Token::LeftParen,
        atom("2"),
        atom("3"),
        Token::RightParen,
        atom("x-y-z")
    ];
    assert_eq!(expected, tokenize("  (asdf(1) ( 2 3 ) ; bar\nx-y-z".to_owned()));
}
