use std::sync::Arc;

use im::hashmap::HashMap;
use im::hashset::HashSet;
use im::vector::Vector;

#[derive(Debug, Clone, PartialEq)]
pub enum Type {
    Const(Arc<String>), // constructor
    Var(Arc<String>),
    App(Arc<Type>, Arc<Type>),
}

impl Type {
    pub fn as_scheme(&self) -> Scheme {
        Scheme{
            vars: HashSet::new(),
            t: self.clone(),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct StructDef {
    name: String,
    type_vars: TypeVars,
    variants: Vector<Variant>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Variant {
    name: String,
    fields: Vector<StructField>,
}

#[derive(Debug, Clone)]
pub struct Constructor {
    pub sch: Arc<Scheme>,
    pub fields: Vector<Type>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct StructField {
    // TODO: Consider adding support for names
    typ: Type,
}

pub fn type_constructor(s: &str) -> Type {
    Type::Const(Arc::new(s.to_owned()))
}

pub fn int_t() -> Type {
    Type::Const(Arc::new("Int".to_owned()))
}

pub fn bool_t() -> Type {
    Type::Const(Arc::new("Bool".to_owned()))
}

pub fn char_t() -> Type {
    Type::Const(Arc::new("Char".to_owned()))
}

pub fn arrow_t() -> Type {
    Type::Const(Arc::new("(->)".to_owned()))
}

pub fn func_t(t1: Type, t2: Type) -> Type {
    t_app(t_app(arrow_t(), t1), t2)
}

pub fn t_var(s: &str) -> Type {
    Type::Var(Arc::new(s.to_owned()))
}

pub fn t_app(t1: Type, t2: Type) -> Type {
    Type::App(Arc::new(t1), Arc::new(t2))
}

#[derive(Debug, Clone)]
pub struct Scheme {
    pub vars: TypeVars,
    pub t: Type,
}

pub type Substitution = HashMap<String, Type>;

pub fn empty_substitution() -> Substitution {
    HashMap::new()
}

pub fn singleton_substitution(var: String, t: Type) -> Substitution {
    HashMap::singleton(var, t)
}

// compose the substitutions as though sub1 as applied first, then sub2 was
// applied after that
pub fn compose(sub1: &Substitution, sub2: &Substitution) -> Substitution {
    let mut result: Substitution = sub1.iter()
        .map(|(k, v)| (k.clone(), v.apply_sub(sub2)))
        .collect();
    for (k, v) in sub2 {
        if !result.contains_key(&*k) {
            result = result.insert(k.clone(), v.clone());
        }
    }
    result
}

pub type Environment = HashMap<String, Scheme>;

pub fn update_env(env: &Environment, var: Arc<String>, sch: Scheme) -> Environment {
    env.insert((*var).clone(), sch)
}

pub type TypeVars = HashSet<String>;

pub trait ApplySub {
    fn apply_sub(&self, sub: &Substitution) -> Self;
}

pub trait FTVs {
    fn ftvs(&self) -> TypeVars;
}

impl ApplySub for Type {
    fn apply_sub(&self, sub: &Substitution) -> Self {
        match self {
            Type::Const(s) =>
                Type::Const(s.clone()),
            Type::Var(fv) =>
                (*sub.get(&**fv).unwrap_or(Arc::new(Type::Var((*fv).clone())))).clone(),
            Type::App(t1, t2) =>
                Type::App(Arc::new(t1.apply_sub(sub)), Arc::new(t2.apply_sub(sub))),
        }
    }
}

impl FTVs for Type {
    fn ftvs(&self) -> TypeVars {
        match self {
            Type::Const(_) =>
                HashSet::new(),
            Type::Var(fv) =>
                HashSet::singleton(fv),
            Type::App(t1, t2) =>
                t1.ftvs().union(t2.ftvs()),
        }
    }
}

impl ApplySub for Scheme where {
    fn apply_sub(&self, sub: &Substitution) -> Self {
        let smaller_sub = sub.iter()
            .filter(|(k, _)| !self.vars.contains(&**k))
            .collect();
        Scheme{
            vars: self.vars.clone(),
            t: self.t.apply_sub(&smaller_sub),
        }
    }
}

impl FTVs for Scheme where {
    fn ftvs(&self) -> TypeVars {
        self.t.ftvs().difference(&self.vars)
    }
}

impl ApplySub for Environment where {
    fn apply_sub(&self, sub: &Substitution) -> Self {
        self.iter().map(|(k, v)| (k.clone(), v.apply_sub(sub))).collect()
    }
}

impl FTVs for Environment where {
    fn ftvs(&self) -> TypeVars {
        let mut tvars = HashSet::new();
        for (_, t) in self {
            tvars = tvars.union(&t.ftvs());
        }
        tvars
    }
}
