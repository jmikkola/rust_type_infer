#[macro_use]
extern crate im;

pub mod compiler;
pub mod parse;
pub mod expression;
pub mod types;
pub mod inference;

fn main() {
    let text = "((fn x x) 1)";
    println!("{:?}", compiler::compile_str(text));
}
