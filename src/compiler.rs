use std::sync::Arc;

use im::hashset::HashSet;
use im::vector::Vector;

use expression;
use inference;
use inference::{Inference};
use parse;
use types;
use types::{ApplySub, Constructor, Environment, Scheme, Type};
use types::{func_t, t_app, t_var, int_t, type_constructor};

#[derive(Debug, PartialEq)]
pub enum CompilationResult {
    ParseErr(parse::Err),
    RecognizeErr(expression::Err),
    TypeErr(inference::Err),
    Success(Vector<(types::Type, expression::Expr)>),
}

use self::CompilationResult::{*};

pub fn compile(text: String) -> CompilationResult {
    let ast_roots = match parse::parse(text) {
        Err(parse_err) => {
            return ParseErr(parse_err);
        },
        Ok(asts) => asts,
    };

    let expressions = match expression::from_asts(ast_roots) {
        Err(expr_err) => {
            return RecognizeErr(expr_err);
        },
        Ok(exprs) => exprs,
    };

    let mut results = Vector::new();

    // ==== TODO: Make this setup automatic from the text ==== //

    let nilScheme = Scheme{
        vars: hashset!["a".to_owned()],
        t: t_app(type_constructor("List"), t_var("a")),
    };

    let consScheme = Scheme{
        vars: hashset!["a".to_owned()],
        t: func_t(
            t_var("a"),
            func_t(
                t_app(type_constructor("List"), t_var("a")),
                t_app(type_constructor("List"), t_var("a"))
            )
        ),
    };

    let mut inf = Inference::new();
    inf.add_constructor(
        "Nil".to_owned(),
        Constructor{
            sch: Arc::new(nilScheme.clone()),
            fields: vector![],
        }
    );
    inf.add_constructor(
        "Cons".to_owned(),
        Constructor{
            sch: Arc::new(Scheme{
                vars: hashset!["a".to_owned()],
                t: t_app(type_constructor("List"), t_var("a")),
            }),
            fields: vector![
                t_var("a"),
                t_app(type_constructor("List"), t_var("a"))
            ],
        }
    );

    let mut env = Environment::new();
    env = types::update_env(&env, arcStr("Nil"), nilScheme);
    env = types::update_env(&env, arcStr("Cons"), consScheme);

    // ==== end setup ==== //

    for expr in expressions {
        match inf.infer_type(&env, expr.clone()) {
            Err(type_err) => {
                return TypeErr(type_err);
            },
            Ok((typ, sub)) => {
                let t = typ.apply_sub(&sub);
                results = results.push_back((t, (*expr).clone()));
            },
        };
    }

    Success(results)
}

pub fn compile_str(text: &str) -> CompilationResult {
    compile(text.to_owned())
}

#[test]
fn test_compile_number() {
    let result = compile_str("123");
    let expr = parse1("123");
    let t = types::int_t();
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_compile_function() {
    let result = compile_str("(fn x x)");
    let expr = parse1("(fn x x)");
    let t = types::func_t(types::t_var("0"), types::t_var("0"));
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_compile_application() {
    let text = "((fn x x) 1)";
    let result = compile_str(text);

    let expr = parse1("((fn x x) 1)");
    let t = types::int_t();
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_compile_let() {
    let text = "(let (id (fn x x)) id)";
    let result = compile_str(text);

    let expr = parse1(text);
    let t = types::func_t(types::t_var("2"), types::t_var("2"));
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_compile_let_and_application() {
    let text = "(let (id (fn x x)) (id id))";
    let result = compile_str(text);

    let expr = parse1(text);
    let t = types::func_t(types::t_var("4"), types::t_var("4"));
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_self_referential_binding() {
    let text = "(let (x x) x)";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::t_var("1"), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_if_simple() {
    let text = "(if true 1 2)";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::int_t(), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_if_forces_same_branch_type() {
    let text = "(let (x x) (if true 2 x))";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::int_t(), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_if_fails_if_test_is_not_boolean() {
    let text = "(if 1 2 3)";
    let result = compile_str(text);
    let expected = TypeErr(inference::Err::CannotUnify);
    assert_eq!(expected, result);
}

#[test]
fn test_if_fails_if_branches_are_different() {
    let text = "(if true true 3)";
    let result = compile_str(text);
    let expected = TypeErr(inference::Err::CannotUnify);
    assert_eq!(expected, result);
}

#[test]
fn test_if_forces_type_of_x() {
    let text = "(fn x (if x x x))";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::func_t(types::bool_t(), types::bool_t()), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_let_generalization() {
    let text = "(let (id (fn x x)) (if (id true) (id 1) (id 2)))";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::int_t(), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_let_generalization_2() {
    let text = "(let (id (fn x x)) (if (id true) (id id) id))";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::func_t(types::t_var("7"), types::t_var("7")), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_mutually_recursive_functions() {
    let text = "
        (let (f (fn x (if true (g x) x))
              g (fn y (if true 1 (f y))))
          f)
    ";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::func_t(types::int_t(), types::int_t()), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_nested_scope() {
    let text = "
        (let (x (fn x x))
          (let (y (x true))
            (let (x y) (if x 1 2))))
    ";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::int_t(), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_nested_let() {
    let text = "
        (let (x (let (x (fn x x)) x))
          x)
    ";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(types::func_t(types::t_var("4"), types::t_var("4")), expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_nested_functions() {
    let text = "
        ((fn x ((fn x x) x))
          (fn x (fn y x)))
    ";
    let result = compile_str(text);

    let expr = parse1(text);
    let t = types::func_t(types::t_var("4"),
                          types::func_t(types::t_var("5"), types::t_var("4")));
    let expected = Success(vector![(t, expr)]);
    assert_eq!(expected, result);
}

#[test]
fn test_patterns() {
    let text = "
        (match ((Cons 123) Nil)
            ((Cons n _) n)
            (Nil 2))
    ";
    let result = compile_str(text);

    let expr = parse1(text);
    let expected = Success(vector![(int_t(), expr)]);
    assert_eq!(expected, result);
}

#[cfg(test)]
fn parse1(s: &str) -> expression::Expr {
    let ast = parse::parse(s.to_owned())
        .unwrap()
        .get(0)
        .unwrap();
    expression::from_ast(ast)
        .unwrap()
}

fn arcStr(s: &str) -> Arc<String> {
    Arc::new(s.to_owned())
}
