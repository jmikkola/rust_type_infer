Rust Type Infer

This is a type inference engine written in Rust.

It finds types for a language that is only a little bit more
complicated than lambda calculus.

## TODO

- Add support for match statements and enum types.
    - Add syntax for match expressions
    - Add syntax for structures for them to match
    - Add a way to look up what type a constructor in a match arm refers to
    - Add inference rules
    - Gather the type statements before typing code
- Add better support for top-level, e.g. topological sort of functions.
- Add a way to annotate nodes in the expression tree with their type.
- Add blocks, variable updates, and return statements.
- Add type classes
    - Add syntax for defining simple type classes and implementations
    - Add type inference for type classes
    - Add predicates to type class implementations and extend the algorithm to
      handle entailment
- Add interpreter?
